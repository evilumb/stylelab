Toggle these modular enhancements via your favorite CSS injector (such as [Stylus](https://add0n.com/stylus.html)).

*  [Less Lab](LessLab.css) - hides non-critical issue details that re-appear on hover/focus-within
   ![Screenshot of Less Lab style](screenshots/LessLab.png "LessLab screenshot")
*  [Blur Lab](BlurLab.css) - Blurs issue titles, reference numbers, author names and profile images
   ![Screenshot of Blur Lab style](screenshots/BlurLab.png "Blur Lab screenshot")
*  [Dark Lab](https://gitlab.com/lorin/GitlabDarkMode) - Experimental dark mode
*  [Wide Lab](WideLab.css) - use all available horizontal space
*  [Column Lab](ColumnLab.css) - Issues split in two columns
*  [Commit Crunch Lab](CommitCrunchLab.css) - Hide buttons/actions, transforms commits list. See source for JS one-liner to hide nit-like commits.